# Spike Report

# AI5- Environment query system

## Introduction

This spike aims to improve apron the AI4 spike by introducing the environmental query system to create smarter ai characters. Although the EQS system is experimental, it allows the ai to find points of interest.

This will be accomplished by delivering on the spike deliverables on time and within scope.

## Goals

1. To deliver on the spike deliverables as per the spike plan.
2. To expand on the understanding gained on in AI spike 4 and gain knowledge on the EQS system, such that the specific areas include&quot;
  1. EQS Queries
  2. Implementing EQS in a behavior tree
  3. Designing behavior trees to use the EQS system
3. To stay within the scope of this spike by following the spike plan.

## Personnel

| Primary – Matthew Dawkins | Secondary – N/A |
| --- | --- |

## Technologies, Tools, and Resources used

Provide resource information for team members who wish to learn additional information from this spike.

- Unreal Engine EQS tutorial series 1 [https://www.youtube.com/watch?v=8WxiTehK6Kc](https://www.youtube.com/watch?v=8WxiTehK6Kc)
- EQS tutorial series 2 [https://www.youtube.com/watch?v=kL0m0jOudQY](https://www.youtube.com/watch?v=kL0m0jOudQY)
- Epic games docco on EQS [https://docs.unrealengine.com/en-US/Engine/ArtificialIntelligence/EQS/index.html](https://docs.unrealengine.com/en-US/Engine/ArtificialIntelligence/EQS/index.html)
- Generating and designing EQS queries [https://mercuna.com/writing-eqs-test/](https://mercuna.com/writing-eqs-test/)
- How to implement structs in ue4 [https://wiki.unrealengine.com/Structs,\_USTRUCTS(),\_They&#39;re\_Awesome](https://wiki.unrealengine.com/Structs,_USTRUCTS(),_They&#39;re_Awesome)
- How to implement interfaces in ue4 [https://wiki.unrealengine.com/Interfaces\_in\_C%2B%2B](https://wiki.unrealengine.com/Interfaces_in_C%2B%2B)
- Further docco on interfaces [https://isaratech.com/ue4-declaring-and-using-interfaces-in-c/](https://isaratech.com/ue4-declaring-and-using-interfaces-in-c/)
- Spawning blueprints from c++ [https://answers.unrealengine.com/questions/53689/spawn-blueprint-from-c.html](https://answers.unrealengine.com/questions/53689/spawn-blueprint-from-c.html)

## Tasks undertaken

One of the first issues in this spike is designing a character that can be tagged and then be &quot;it&quot; or be &quot;it&quot; and then tag some one and then be &quot;notIt&quot;. In order to accomplish these two characters were made one being the predator (It character) and the other being the prey character (the not it character). As for each ai npc, they were each represented by a data struct, that way the character/ controller could then be swapped easily from an external object and the data transferred. The major steps taken to do this will now be outlined:

**Data management and Constants file**

_In order to create the data needed for this project a constants object was created that extends UBluePrintFunctionLibrary and acted as a container for all data types. Thus, in the header we create an empty class_

![](reportImages\1.png)

_And then outside the file we create the data types to be used, first an enum (as documented in previous spikes)_

![](reportImages\2.png)

_And then the npc data struct that will represent an individual npc_

![](reportImages\3.png)

**Character switching mechanic**

_In one of the characters we must bind a hit event to tagged function that switches the it character to not it and then the not it character to it. This makes most sense to be put in the predator as it is its job to tag other npcs, thus in the header_

![](reportImages\4.png)

_And then in the cpp file in begin play function_

![](reportImages\5.png)

_And then finally we create the functionality in the tagged function itself_

![](reportImages\6.png)

_Now that we have a way of changing the state of each npc (in terms of data), we must now create a way to change the characters based on the state stored in the data struct. Thus in the controller we can add a check that then call a function in an external object. In the controller header_

![](reportImages\7.png)







_Then in the cpp file_

![](reportImages\8.png)

_Then we just call this function every tick. Note the change character function being called from the game mode, this is a function that pulls the data from this character then destroys it, then creates a new instance of class specified (ie preditorBP) and finally writes the data to the newly spawn character. In effect keeping the data the same but swapping the character. To do this in the game mode header we add_

![](reportImages\9.png)

_And then in the cpp file we add the following functionality_

![](reportImages\10.png)

_Noting that as soon as we call the Destoy() function we can no longer access any object related to the destroyed object._

**BlackBoard and Behaviour tree**

As with previous spikes we create a behavior tree and black board that creates the desired ai mechanic we are trying to model. This has been explored in previous spikes so only the major components will be outlined, however for this implementation we created two sets one for the it npc and one for the not it npc.

Creating the black board and behavior tree for the preditor or it npc

![](reportImages\11.png)

Then the behavior tree noting that the EQS Queries will be covered in the next section

![](reportImages\12.png)

Finally the get random location (which has been completed in blueprints)

![](reportImages\13.png)

Then for the prey we create similar set of controlling blueprints

![](reportImages\14.png)

Then for the black board

![](reportImages\15.png)

Note that for both of these implementations the two blueprint controllers were created by extending the custom c++ controller, setting it to the default for each blueprint character then adding the following

![](reportImages\16.png)



**EQS Queries**

We can start by creating a simple query to place an eqs object on an actor then see if querier can see the object. To do this we must

1. Create a new query
2. Add a generate set of actor&#39;s node
3. Add a trace test

The final product should look like this

![](reportImages\17.png)

This can then be used for both the predator and prey.

The second type of query that we need is to find a hiding place, to accomplish this we must first create a custom context. This can be accomplished by creating a new blueprint class selecting the context as the perent

![](reportImages\18.png)

Then in our new context we need to add the following to return all the actors that we need to hide from.

![](reportImages\19.png)

After we have created this context, we can then use it to create a query to find the location that is hidden from the character that is &quot;it&quot;. To do this we must

1. Create a grid around the character
2. Create an inverse trace to the context we just create (check if the spot can be seen)
3. Give a weight to the distance away from the &quot;it&quot; character
4. Give a weight to the distance away from the not it character (find the closest one)
5. Check that the spot is reachable

![](reportImages\20.png)

We can these use these quires to find the locations for the behavior tree and move the characters accordingly.



## What we found out

The knowledge gained in this spike report has encompassed several different areas for example:

- Character switching: the use of data structs and interchangeable character controller combos, how to accomplish this and how to prevent this mechanic from causing crashes.
- EQS Queries: the creation and use in behaviour trees to create complex logic by doing test on an initial query.
- EQS Context blueprints: used to return a specific set of actors for the EQS to search for.
- Interfacing blueprints and C++ code to produce complex game mechanics and objects

Overall a better knowledge of creating complex ai was obtained and how to achieve this through the EQS system. Further the use of flexible objects such as the custom controller that can then be used for the it and not it characters was also an interesting approach which was unplanned but still interesting none the less.

##  Open Issues/risks

It is noted that there is currently a bug with the construction helper&#39;s library that causes the loading of the project to get stuck. This is most likely due to the references being null for some reason, I was unable to resolve this issue however I did come up with a work around which is documented in a text file stored in the project folder.

## Recommendations

Although this project did investigate the EQS system there are many different other areas that could be used and a spike into the different uses of this system might be beneficial.