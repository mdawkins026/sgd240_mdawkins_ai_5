#

# Spike Plan

# AI 5 – Environment Query System

## Context

We can pre-determine how our AI thinks now, but what if we want it to be more dynamic?

Unreal has an &#39;experimental&#39; system called the Environment Query System, which is a very powerful way of finding points of interest. We should learn how to use it.

## Grade Level

Credit

## Gap

1. Tech: How can Unreal Engine agents learn about their environment?
2. Knowledge: What is the Environment Query System?
3. Skill: Creating agents which can reason about the world around them!

## Goals/Deliverables

- The Spike Report should answer each of the Gap questions

Create a new project, which can re-use some of your older code if required.

- Let&#39;s play Tag!
- Create a Character class which has an Enum to be &quot;it&quot; or &quot;not-it&quot;
  - Using the Construction Script (blueprint child class),
  - Set not-it&#39;s to be Blue
  - Set it&#39;s to be Red
- Set up AI Controllers which:
  - When a Blue sees a Red, they Run and Hide
  - When a Red sees a Blue, they Run and &quot;Tag&quot;
    - &quot;Tag&quot; converts a Blue to a Red
  - When they can&#39;t see the other colour, they move to a random point at least X units away (to try and find another
- Create a level with multiple obstacles, and a moderate number of these characters – and a single &quot;it&quot; character in the middle.

## Dates

| Planned start date: | Week 11 |
| --- | --- |
| Deadline: | Week 12 |

## Planning Notes

1. Start with the [Unreal Engine EQS Quick Start Guide](https://docs.unrealengine.com/latest/INT/Engine/AI/EnvironmentQuerySystem/QuickStart/index.html)
2. Use the EQS Testing Pawn to test your queries
3. Use the Gameplay Debugger to test your AI

FOR INTERACES IN 4.22 [https://answers.unrealengine.com/questions/895365/ue4-422-c-interface-implementation.html](https://answers.unrealengine.com/questions/895365/ue4-422-c-interface-implementation.html)