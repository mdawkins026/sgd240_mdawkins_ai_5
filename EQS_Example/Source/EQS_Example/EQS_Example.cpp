// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "EQS_Example.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, EQS_Example, "EQS_Example" );
 