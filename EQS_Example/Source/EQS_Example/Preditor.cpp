// Fill out your copyright notice in the Description page of Project Settings.


#include "Preditor.h"
#include "Constants.h"
#include "Components/CapsuleComponent.h"
#include "EQS_ExampleCharacter.h"
#include "CharacterAiControler.h"
#include "Prey.h"

// Sets default values
APreditor::APreditor()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	InternalState = ECharacterState::CS_Preditor;
}

// Called when the game starts or when spawned
void APreditor::BeginPlay()
{
	Super::BeginPlay();
	
	UCapsuleComponent* rootcomp = GetCapsuleComponent();
	rootcomp->OnComponentHit.AddDynamic(this, &APreditor::OnTagged);
}

// Called every frame
void APreditor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void APreditor::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

ECharacterState APreditor::GetType_Implementation()
{
	return InternalState;
}

void APreditor::OnTagged(UPrimitiveComponent * HitComponent, AActor * OtherActor, UPrimitiveComponent * OtherComponent, FVector NormalImpulse, const FHitResult & Hit)
{
	if (!tagged) 
	{
		UE_LOG(LogTemp, Warning, TEXT("OnTagged is being hit :)"));
		if (OtherActor->GetClass()->IsChildOf(APrey::StaticClass()))
		{
			UE_LOG(LogTemp, Warning, TEXT("I HAVE HIT THE TARGET :)"));

			// Change the preditor to the prey
			ACharacterAiControler* MyControler = (ACharacterAiControler*)(GetController());
			MyControler->data.CurrentCharacterState = ECharacterState::CS_Prey;

			// Change the Prey to the preditor
			APrey* TaggedPrey = (APrey*)OtherActor;
			ACharacterAiControler* taggedControler = (ACharacterAiControler*)TaggedPrey->GetController();
			taggedControler->data.CurrentCharacterState = ECharacterState::CS_Preditor;

			tagged = true;
		}
	}
}

