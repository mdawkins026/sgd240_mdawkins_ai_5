// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Object.h"
#include "UObject/Interface.h"
#include "Constants.h"
#include "CharacterInterface.generated.h"

// This class does not need to be modified.
UINTERFACE(Blueprintable)
class UCharacterInterface : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class EQS_EXAMPLE_API ICharacterInterface
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:

	// Returns the type of the character in the form of ECharacterState
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	ECharacterState GetType();
};
