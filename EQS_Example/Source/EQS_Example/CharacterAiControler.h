// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "Constants.h"
#include "CharacterAiControler.generated.h"

/**
 * 
 */
UCLASS()
class EQS_EXAMPLE_API ACharacterAiControler : public AAIController
{
	GENERATED_BODY()

	ACharacterAiControler();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// called on the possesion of a character
	virtual void OnPossess(APawn * InPawn) override;

	// Data and properties

	FCharacterData data;

	UPROPERTY(Transient)
	class UBlackboardComponent* BlackboardComp;

	UPROPERTY(Transient)
	class UBlackboardData* BlackBoardDataAsset;

	UPROPERTY(Transient)
	class UBehaviorTreeComponent* BehaviourTreeComp;

	UPROPERTY(EditAnywhere, Category = "BehaviourTree")
	class UBehaviorTree* AiCharactersBehavior;

	// Functions
	UFUNCTION(BlueprintSetter, Category = "CharacterData")
	void SetTaged(ECharacterState NewState);

	UFUNCTION()
	void CheckState();

	// BP objects to be toggled between
	TSubclassOf<class APreditor> PreditorBP;
	TSubclassOf<class APrey> PreyBP;

	UPROPERTY(BlueprintReadWrite, Category = "BehaviourTree")
	bool IsIt;

	bool InitCoolDown = true;

	// Inital wait timmer handler
	FTimerHandle InitWaitTimerHandeler;

	// Init wait event function
	void OnTimeToTag();
};
