// Fill out your copyright notice in the Description page of Project Settings.


#include "CharacterAiControler.h"
#include "UObject/ConstructorHelpers.h"
#include "Constants.h"
#include "CharacterInterface.h"
#include "Preditor.h"
#include "Prey.h"
#include "Engine/Blueprint.h"
#include "Engine/World.h"
#include "EQS_ExampleGameMode.h"
#include "TimerManager.h"

ACharacterAiControler::ACharacterAiControler()
{
	//ConstructorHelpers::FObjectFinder<UBehaviorTree> newAsset(TEXT("INSERT PATH HERE "));
	//AiCharactersBehavior = newAsset.Object;

	// initalise the character data
	data = FCharacterData();
	data.ControlerReference = this;

	// Debug code
	//data.CurrentCharacterState = ECharacterState::CS_Prey;


	// Get the two bp character
	static ConstructorHelpers::FClassFinder<APreditor> PreditorBPObj(TEXT("Class'/Game/AI/PreditorBP.PreditorBP_C'"));
	PreditorBP = (UClass*)PreditorBPObj.Class;
	static ConstructorHelpers::FClassFinder<APrey> PreyBPObj(TEXT("Class'/Game/AI/PreyBP.PreyBP_C'"));
	PreyBP = (UClass*)PreyBPObj.Class;

}

void ACharacterAiControler::BeginPlay()
{
	Super::BeginPlay();
}

void ACharacterAiControler::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if (InitCoolDown == false) 
	{
		CheckState();
	}
}

void ACharacterAiControler::OnPossess(APawn * InPawn)
{
	Super::OnPossess(InPawn);

	// set the current actor reference to the incoming pawn
	data.CurrentActorReference = InPawn;
	if (InPawn->GetClass()->IsChildOf(APrey::StaticClass())) 
	{
	}
	else if (InPawn->GetClass()->IsChildOf(APreditor::StaticClass()))
	{
		data.CurrentCharacterState = ECharacterState::CS_Preditor;
	}

	GetWorld()->GetTimerManager().SetTimer(InitWaitTimerHandeler, this, &ACharacterAiControler::OnTimeToTag, 5.0f, false);
}

void ACharacterAiControler::SetTaged(ECharacterState NewState)
{
	data.CurrentCharacterState = NewState;
}

void ACharacterAiControler::CheckState()
{
	if (data.CurrentActorReference != nullptr) 
	{
		//UE_LOG(LogTemp, Warning, TEXT("Check being called"));
		if (data.CurrentActorReference->GetClass()->ImplementsInterface(UCharacterInterface::StaticClass()))
		{
			//UE_LOG(LogTemp, Warning, TEXT("Interface check passed"));
			ECharacterState state = ICharacterInterface::Execute_GetType(data.CurrentActorReference);

			if (data.CurrentCharacterState != state)
			{
				//UE_LOG(LogTemp, Warning, TEXT("Changing characater"));

				if (data.CurrentCharacterState == ECharacterState::CS_Preditor)
				{
					//UE_LOG(LogTemp, Warning, TEXT("Preditor"));
					IsIt = true;


					((AEQS_ExampleGameMode*)this->GetWorld()->GetAuthGameMode())->ChangeCharacter(PreditorBP, this);
				}
				else
				{
					//UE_LOG(LogTemp, Warning, TEXT("Prey"));
					IsIt = false;

					((AEQS_ExampleGameMode*)this->GetWorld()->GetAuthGameMode())->ChangeCharacter(PreyBP, this);
				}
			}
		}
	}
}

void ACharacterAiControler::OnTimeToTag()
{
	InitCoolDown = false;
}
