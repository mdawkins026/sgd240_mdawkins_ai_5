// Fill out your copyright notice in the Description page of Project Settings.


#include "Prey.h"
#include "Constants.h"
#include "Components/CapsuleComponent.h"
#include "EQS_ExampleCharacter.h"
#include "CharacterAiControler.h"

// Sets default values
APrey::APrey()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	InternalState = ECharacterState::CS_Prey;
}

// Called when the game starts or when spawned
void APrey::BeginPlay()
{
	Super::BeginPlay();
	
	//Debug Code
	//UCapsuleComponent* rootcomp = GetCapsuleComponent();
	//rootcomp->OnComponentHit.AddDynamic(this, &APrey::OnTagged);
}

// Called every frame
void APrey::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void APrey::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

ECharacterState APrey::GetType_Implementation()
{
	return InternalState;
}

// Debug Version 
//void APrey::OnTagged(UPrimitiveComponent * HitComponent, AActor * OtherActor, UPrimitiveComponent * OtherComponent, FVector NormalImpulse, const FHitResult & Hit)
//{
//	UE_LOG(LogTemp, Warning, TEXT("OnTagged is being hit :)"));
//	if (OtherActor->GetClass()->IsChildOf(AEQS_ExampleCharacter::StaticClass())) 
//	{
//		UE_LOG(LogTemp, Warning, TEXT("I HAVE HIT THE TARGET :)"));
//		ACharacterAiControler* MyControler = (ACharacterAiControler*)(GetController());
//		MyControler->data.CurrentCharacterState = ECharacterState::CS_Preditor;
//	}
//}

