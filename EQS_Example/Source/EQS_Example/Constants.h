// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "Misc/Guid.h"
#include "Constants.generated.h"

/**
 * 
 */
UCLASS()
class EQS_EXAMPLE_API UConstants : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
	
};

// The state of the character

UENUM(BlueprintType)		
enum class ECharacterState : uint8
{
	CS_Prey 	UMETA(DisplayName = "Prey"),
	CS_Preditor	UMETA(DisplayName = "Preditor")
};

// The data structure for each character controler

USTRUCT(BlueprintType)
struct FCharacterData
{
	// By Default sets random id, null charcater ref, character state as CS_Prey and (0,0,0) as character location
	GENERATED_BODY()

	UPROPERTY(BlueprintReadOnly, category = "CharacterData")
	FGuid Id;

	UPROPERTY(BlueprintReadOnly, category = "CharacterData")
	AActor* CurrentActorReference;

	UPROPERTY(BlueprintReadOnly, category = "CharacterData")
	class ACharacterAiControler* ControlerReference;

	UPROPERTY(BlueprintReadOnly, category = "CharacterData")
	ECharacterState CurrentCharacterState;

	//Constructor
	FCharacterData()
	{
		// intialise data
		Id = FGuid::NewGuid();
		CurrentActorReference = nullptr;
		ControlerReference = nullptr;
		CurrentCharacterState = ECharacterState::CS_Prey;
	}
};
