// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "EQS_ExampleGameMode.generated.h"


UCLASS(minimalapi)
class AEQS_ExampleGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AEQS_ExampleGameMode();

	void ChangeCharacter(UClass* NewCharacterClass, class ACharacterAiControler* controler);

	float score = 0;
};



