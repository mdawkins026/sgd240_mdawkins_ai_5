// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "EQS_ExampleGameMode.h"
#include "EQS_ExampleCharacter.h"
#include "CharacterAiControler.h"
#include "UObject/ConstructorHelpers.h"
#include "Constants.h"
#include "CharacterInterface.h"
#include "Preditor.h"
#include "Prey.h"
#include "Engine/Blueprint.h"
#include "Engine/World.h"

AEQS_ExampleGameMode::AEQS_ExampleGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}

void AEQS_ExampleGameMode::ChangeCharacter(UClass * NewCharacter, ACharacterAiControler * controler)
{
	score = score + 1.0f;

	// Pull the data from the existing controller
	if (controler != nullptr) 
	{
		FVector CurrentLocation = (controler->GetPawn())->GetActorLocation();
		FRotator CurrentRotation = (controler->GetPawn())->GetActorRotation();
		FActorSpawnParameters SpawnInfo;
		FCharacterData data = controler->data;
		AActor* oldPawn = controler->GetPawn();

		// kill the old pawn and controler
		//controler->UnPossess();
		oldPawn->Destroy();

		// spawn a new preditor or prey
		ACharacter* NewCharacterOBj = GetWorld()->SpawnActor<ACharacter>(NewCharacter, CurrentLocation, CurrentRotation, SpawnInfo);

		if (NewCharacterOBj != nullptr)
		{
			// send the data to the new controler
			data.CurrentActorReference = NewCharacterOBj;
			NewCharacterOBj->SpawnDefaultController();
			ACharacterAiControler* newControler = (ACharacterAiControler*)NewCharacterOBj->GetController();
			if (newControler != nullptr)
			{
				newControler->data = data;
			}
		}
	}
}
